<?php


namespace App\Traits;


use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait UserHasAcess
{


    public  function hasAccess($action){


        $userPolicies = DB::table('user_policies')
            ->select(['permissions.name','user_policies.has_access'])
            ->join('permissions','user_policies.permissions_id','=','permissions.id')
            ->distinct()
            ->get();

        $toAdd = $userPolicies->where('has_access',1)->pluck('name');
        $toDel = $userPolicies->where('has_access',0)->pluck('name');


        $permissions = DB::table('user_has_roles')
            ->select(['permissions.name'])
            ->join('roles','user_has_roles.role_id','=','roles.id')
            ->join('role_has_permissions','roles.id','=','role_has_permissions.role_id')
            ->join('permissions','role_has_permissions.permission_id','=','permissions.id')
            ->where('user_has_roles.user_id','=',$this->getAuthIdentifier())
            ->get()
            ->pluck('name')
            ->union($toAdd)
            ->diff($toDel)
            ->values()
            ->toJson();

        $permisosEncriptados = Crypt::encryptString($permissions);

        Log::info($permisosEncriptados);

        $json = Crypt::decryptString($permisosEncriptados);

        Log::info($json);

        Log::info(json_decode($json));


        return true; //in_array($action,$permissions);

    }

}
