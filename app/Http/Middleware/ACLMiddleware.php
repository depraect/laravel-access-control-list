<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ACLMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $routeName = $request->route()->getName();

        if($request->user()->isSuperAdmin() || $request->user()->hasAccess($routeName)){
            return $next($request);
        }

        abort(403,'No tiene accesso');

    }
}
