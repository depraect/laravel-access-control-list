<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function login(LoginRequest  $request){

        $credentials = $request->only(['email','password']);

        if(Auth::attempt($credentials)){
            $token  = Auth::user()->createToken('token')->plainTextToken;
            return response()->json([
                'token'=>$token,
                'type'=>'Bearer'
            ]);
        }

        abort(401,"Unauthorized");


    }


}
